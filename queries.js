exports.getProductsQuery = `query {     
    products(first: 50) {
      edges {
        node {
          availableForSale
          createdAt
          description
          descriptionHtml
          handle
          id
          productType
          publishedAt
          requiresSellingPlan
          title
          updatedAt
          vendor
          onlineStoreUrl
          collections(first: 5) {
            edges {
              node {
                description
                descriptionHtml
                handle
                id
                title
                updatedAt
              }
            }
          }
          compareAtPriceRange {
            maxVariantPrice {
              amount
              currencyCode
            }
            minVariantPrice {
              amount
              currencyCode
            }
          }
          media(first: 5) {
            edges {
              node {
                alt
                mediaContentType
              }
            }
            pageInfo {
              hasNextPage
              hasPreviousPage
            }
          }
          options {
            id
            name
            values
          }
          tags
          variants(first:5){
            edges {
              node {
                available
                availableForSale
                compareAtPrice
                currentlyNotInStock
                product {
                  availableForSale
                  createdAt
                  description
                  handle
                  descriptionHtml
                  tags
                  title
                  totalInventory
                  updatedAt
                  vendor
                }
                requiresShipping
                title
                weight
                weightUnit
              }
            }
          }
        }
      }
    } 
}
`;
