const gql = require("graphql-tag");
const ApolloClient = require("apollo-client").ApolloClient;
const fetch = require("node-fetch");
const createHttpLink = require("apollo-link-http").createHttpLink;
const setContext = require("apollo-link-context").setContext;
const InMemoryCache = require("apollo-cache-inmemory").InMemoryCache;

require("dotenv").config();

const httpLink = createHttpLink({
  uri: `https://${process.env.SHOP_NAME}.myshopify.com/api/2021-07/graphql.json`,
  fetch: fetch,
});

const authLink = setContext((_, { headers }) => {
  const authHeaders = {
    "X-Shopify-Storefront-Access-Token": `${process.env.ACCESS_TOKEN}`,
  };
  return {
    headers: authHeaders,
  };
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

const query = async (querystr, variables) => {
  const query = gql(querystr);
  try {
    const result = await client.query({
      query,
      variables,
    });
    return result;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

module.exports = {
  query: query,
};
