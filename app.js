const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

const apollo = require("./apollo");
const queries = require('./queries');

app.use(express.json());

app.get('/',(req,res)=>{
    res.send('Go to /getProducts');
})

app.get('/getProducts',async (req,res)=>{
    try{
        const result = await apollo.query(queries.getProductsQuery);
        res.send(result);
    }
    catch(e){
        res.send({Error:e.message});
    }
})

app.listen(port, () => console.log(`App running on port ${port}`));